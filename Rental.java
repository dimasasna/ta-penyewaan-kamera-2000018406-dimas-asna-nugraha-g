package com.company;

import java.util.Scanner;

public class Rental {
    private double uang;
    int hari;
    int harga;
    int DSLR;
    double Mirrorless;
    Scanner input = new Scanner(System.in);
    //    konstructor over loading rental untuk DSLR
    public Rental(int DSLR){
        this.DSLR=DSLR;
        System.out.println("Sewa perhari : "+DSLR);
    }
    //    konstructor over loading rental untuk Mirrorless
    public Rental(int Mirrorless, int hari){
        this.Mirrorless=Mirrorless;
        this.hari=hari;
        int byr=Mirrorless*hari;
        System.out.println("Sewa perhari : "+byr);
    }
    public static void pilihan(){
        Scanner input = new Scanner(System.in);
        System.out.println();
        System.out.println("Silahkan pilih Jenis Kamera :");
        System.out.println("1. DSLR");
        System.out.println("2. Mirrorless");
        System.out.print("Masukkan pilihan : ");
        int pilih = input.nextInt();
        switch(pilih){
            case 1:
                Rental.DLSR();
                break;
            case 2:
                Rental.Mirrorless();
                break;
        }
    }
    //    methode DSLR
    public static void DLSR(){
        Scanner input = new Scanner(System.in);
        System.out.println();
        System.out.println("======================");
        System.out.println("Pilih Jenis DSLR");
        System.out.println("======================");
        System.out.println("1.Canon EOS 5D Mark III.");
        System.out.println("2.Nikon D5200.");
        System.out.println("3.Fujifilm FiePix HS25EXR.");
        System.out.println("4.Panasonic Lumix GH5.");
        System.out.print("Masukkan pilihan : ");
        int pilih1=input.nextInt();
        switch(pilih1){
            case 1:
                Rental canon=new Rental(150000);
                System.out.println("Type Canon EOS 5D Mark III.");
                canon.tampilDSLR();
                break;
            case 2:
                Rental nikon=new Rental(120000);
                System.out.println("Type Nikon D5200.");
                nikon.tampilDSLR();
                break;
            case 3:
                Rental fujifilm=new Rental(125000);
                System.out.println("Type Fujifilm FiePix HS25EXR.");
                fujifilm.tampilDSLR();
                break;
            case 4:
                Rental panasonic=new Rental(170000);
                System.out.println("Type Panasonic Lumix GH5.");
                panasonic.tampilDSLR();
                break;
        }
    }
    //    methode Mirrorless
    public static void Mirrorless(){
        Scanner input = new Scanner(System.in);
        System.out.println();
        System.out.println("======================");
        System.out.println("Pilih Jenis Mirrorless ");
        System.out.println("======================");
        System.out.println("1.Sony A6000.");
        System.out.println("2.Canon EOS M50 II.");
        System.out.println("3.Panasonic Lumix DMC-LX100 GCK.");
        System.out.println("4.Fujifilm X-A5.");
        System.out.println("5.Nikon 1 J5 Double Kit.");
        System.out.print("Masukka pilihan : ");
        int pilih2=input.nextInt();
        switch(pilih2){
            case 1:
                Rental msony=new Rental(200000,1);
                System.out.println("Type Sony A6000.");
                msony.tampilMirrorless();
                break;
            case 2:
                Rental mcanon=new Rental(230000,1);
                System.out.println("Type Canon EOS M50 II.");
                mcanon.tampilMirrorless();
                break;
            case 3:
                Rental mpanasonic=new Rental(250000,1);
                System.out.println("Type Panasonic Lumix DMC-LX100 GCK.");
                mpanasonic.tampilMirrorless();
                break;
            case 4:
                Rental mfujifilm=new Rental(210000,1);
                System.out.println("Type Fujifilm X-A5.");
                mfujifilm.tampilMirrorless();
                break;
            case 5:
                Rental mnikon=new Rental(290000,1);
                System.out.println("Type Nikon 1 J5 Double Kit.");
                mnikon.tampilMirrorless();
        }
    }
    void tampilDSLR(){
        int jumlahhariDSLR;
        Scanner input = new Scanner(System.in);
        System.out.println();
        System.out.print("lama sewa:");
        int jmhari=input.nextInt();
        jumlahhariDSLR=jmhari*DSLR;
        System.out.print("Anda memilih kamera DSLR ");
        System.out.println("Dengan harga sewa : "+jumlahhariDSLR);
    }
    void tampilMirrorless(){
        int jumlahhari;
        Scanner input = new Scanner(System.in);
        System.out.println();
        System.out.print("Lama sewa :");
        int jmhari=input.nextInt();
        jumlahhari=(int) (jmhari*Mirrorless*hari);
        System.out.print("Anda memilih kamera Mirrorless ");
        System.out.println("Dengan harga sewa : "+jumlahhari);
    }
}
